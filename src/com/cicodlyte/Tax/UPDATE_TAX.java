package com.cicodlyte.Tax;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.cicodlyte.base.TestBase;
import com.cicodlyte.utility.Utility;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class UPDATE_TAX extends TestBase {

    @Test
    public void UPDATE_TAX() throws IOException, InterruptedException {
        test = extent.createTest("UPDATE TAX");
        WebDriverManager.firefoxdriver().setup();
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.cicod.com/login");

        //Login
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Utility.fetchLocator("shop_XPATH"))).sendKeys(Utility.fetchLocator("Shopname_TEXT"));
        driver.findElement(By.xpath(Utility.fetchLocator("Email_XPATH"))).sendKeys(Utility.fetchLocator("Email_TEXT"));
        driver.findElement(By.xpath(Utility.fetchLocator("Password_XPATH"))).sendKeys(Utility.fetchLocator("Password_TEXT"));
        driver.findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        test.log(Status.PASS, "Login Was Successfull");

        //COM
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Utility.fetchLocator("com_XPATH"))).click();
        test.log(Status.PASS, "COM button fully functional");

        driver.findElement(By.xpath(Utility.fetchLocator("TaxBtn_XPATH"))).click();
        driver.findElement(By.xpath(Utility.fetchLocator("TaxActionbtn_XPATH"))).click();
        driver.findElement(By.xpath(Utility.fetchLocator("TaxUdatebtn_XPATH"))).click();
        test.log(Status.PASS, "Tax Update Button fully Functional");

        driver.findElement(By.xpath(Utility.fetchLocator("TaxName_XPATH"))).clear();
        driver.findElement(By.xpath(Utility.fetchLocator("TaxName_XPATH"))).sendKeys(Utility.fetchLocator("TaxName_TEXT"));

        driver.findElement(By.xpath(Utility.fetchLocator("TaxValue_XPATH"))).clear();
        driver.findElement(By.xpath(Utility.fetchLocator("TaxValue_XPATH"))).sendKeys(Utility.fetchLocator("TaxValue_TEXT"));

        driver.findElement(By.xpath(Utility.fetchLocator("TaxSavebtn_XPATH"))).click();

        Thread.sleep(1000);
        if (driver.findElements(By.xpath(Utility.fetchLocator("AssertUpdateTax_XPATH"))).size() != 0) {
            test.log(Status.PASS, "Tax was Updted Successfully");

        } else {
            test.log(Status.FAIL, "Tax Wasnt Updated");
        }

        String extentReportImageqm11 = System.getProperty("user.dir") + "\\ScreenShot\\ScreenShot" + System.currentTimeMillis() + ".png";
        File srcam11 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            // now copy the screenshot to desired location using copyFile method
            FileUtils.copyFile(srcam11, new File(extentReportImageqm11));
            test.log(Status.INFO, "Screenshot from : " + extentReportImageqm11, MediaEntityBuilder.createScreenCaptureFromPath(extentReportImageqm11).build());
        } catch (IOException e) {
            System.out.println("Error in the captureAndDisplayScreenShot method: " + e.getMessage());
        }

        System.out.println("********************UPDATE TAX TEST IS COMPLETED********************");
        driver.quit();
    }
}
