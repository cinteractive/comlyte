package com.cicodlyte.utility;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.cicodlyte.base.TestBase;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.openqa.selenium.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.io.FileUtils.copyFile;

public class Utility extends TestBase {

	public static String fetchLocator(String key ) throws IOException {

		FileInputStream file = new FileInputStream("./Config/Locators.properties");
		Properties property = new Properties();
		property.load(file);
		return 	property.get(key).toString();
	}

}
